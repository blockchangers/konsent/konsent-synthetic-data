const validator = require('csv-file-validator');
const fs = require('fs');

const requiredError = (headerName, rowNumber, columnNumber) => {
  return `${headerName} is required in the ${rowNumber} row ${columnNumber} column`;
};

const validateError = (headerName, rowNumber, columnNumber) => {
  return `${headerName} is not valid in the ${rowNumber} row ${columnNumber} column`;
};

const uniqueError = (headerName) => {
  return `${headerName} is not unique`;
};

const isEmailValid = (email) => {
  const reqExp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
  return reqExp.test(email);
};

const isPasswordValid = (password) => {
  return password.length >= 4;
};

const CSVConfig = {
  headers: [
    { name    : 'Id',         inputName: 'id',          required: true, requiredError },
    { name    : 'First Name', inputName: 'firstName',   required: true, requiredError },
    { name    : 'Last Name',  inputName: 'lastName',    required: true, requiredError },
    { name    : 'Email',      inputName: 'email',       required: true, requiredError, validate: isEmailValid, validateError },
    { name    : 'Phone',      inputName: 'phone',       required: true, requiredError }
  ]
};

const csvFile  = fs.readFileSync(process.argv[2], 'utf8');

validator(csvFile, CSVConfig)
.then(csvData => {
  console.log(csvData.inValidMessages)
  console.log(csvData.data)
})
.catch(err => {
  console.error(err);
})


